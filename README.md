### What is this repository for? ###

* Engineer Project
* 0.82
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download Dist directory
* start jar file
* configure location of data files
* enjoy

## Versions ##

0.9 - Work in Progress
* selection of time range during measurement creation
* 

0.82
* Changed look and feel of GUI
* Fixed some display issues
## Under development ##
* Assemblage charts features
* generating measurements from templates(Assemblage chart generation does not work yet, minor issues with other generation)