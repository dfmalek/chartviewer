/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Storage;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author norrec
 */
public class DataObject
{
    private String filePath_;
    private int position_;
    private ArrayList<Double> data_;
    
    public DataObject(String filePath, int position, ArrayList<Double> data)
    {
        filePath_ = filePath;
        position_ = position;
        data_ = data;
    }
    
     @Override
    public String toString()
    {
        return Integer.toString(position_) + " " + new File(filePath_).getName();
    }
    
    public String getFilePath()
    {
        return filePath_;
    }
    
    public int getPosition()
    {
        return position_;
    }
    
    public ArrayList<Double> getData()
    {
        return data_;
    }
}
