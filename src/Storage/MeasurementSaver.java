/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Storage;

import CoreElements.Measurement;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author dmalek
 */
public class MeasurementSaver
{
    public static boolean saveMeasurement(Measurement measurement, String dataLocation)
    {
        /*
        String appLocation = System.getProperty("user.dir");
        String osName = System.getProperty("os.name").toLowerCase();
        if(osName.indexOf("win") >= 0)
        {
            appLocation = appLocation + "/Data/Measurements/"; 
        }
        else
        {
            appLocation = appLocation + "I have no idea yet!";
        }*/
        String appLocation = dataLocation + "/Measurements/ " + measurement.getName() + ".cfg";
        try
        {
            File file = new File(appLocation);
            if (!file.exists())
            {
	        file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(measurement.getName());
            bw.newLine();
            bw.write(measurement.getDescription());
            bw.newLine();
            bw.write("*");
            bw.newLine();
            for (int i = 0; i < measurement.getChartsSize(); i++)
            {
                bw.write(measurement.getChartObject(i).getFileString());
                bw.newLine();
            }
            bw.write("*");
            bw.newLine();
            bw.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return true;
    }
    
    public static void saveMeasurementAsTemplate(Measurement measurement, String dataLocation)
    {
        String appLocation = dataLocation + "/Templates/ " + measurement.getName() + ".mtp";
        try
        {
            File file = new File(appLocation);
            if (!file.exists())
            {
	        file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("MEASUREMENT TEMPLATE");
            bw.newLine();
            bw.write(measurement.getName());
            bw.newLine();
            bw.write("*");
            bw.newLine();
            for (int i = 0; i < measurement.getChartsSize(); i++)
            {
                bw.write(measurement.getChartObject(i).getTemplate());
                bw.newLine();
            }
            bw.write("*");
            bw.newLine();
            bw.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
