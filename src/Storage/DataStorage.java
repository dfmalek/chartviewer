/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Storage;

import CoreElements.ReaderHelpers;
import java.util.ArrayList;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author norrec
 */
public class DataStorage
{
    private ArrayList<DataObject> storage_;
    
    public DataStorage(ArrayList<String> files)
    {
        storage_ = new ArrayList();
        for(int i = 0; i < files.size(); i++)
        {
            ArrayList< ArrayList<Double> > tempData = ReaderHelpers.readFile(files.get(i));
            for(int j = 0; j < tempData.size(); j++)
            {
                System.out.println(files.size() + "  " + i + " " + j + tempData.size());
                System.out.println("Added next element " + i + "  " + j);
                DataObject dO = new DataObject(files.get(i), j, tempData.get(j)); 
                storage_.add(dO);
            }
        }
        
        System.out.println("JEST MOC " + storage_.size());
    }
    
    public DataObject getDataObject(int position)
    {
        return storage_.get(position);
    }
    
    public DefaultComboBoxModel getComboBoxModel()
    {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(storage_.toArray());
        return comboBoxModel;
    }
    
    public DefaultComboBoxModel getTimeComboBoxModel()
    {
        ArrayList<DataObject> timeData = new ArrayList();
        for( int i = 0; i < storage_.size() ; i++ )
        {
            if(storage_.get(i).getPosition() == 0)
            {
                timeData.add(storage_.get(i));
            }
        }
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(timeData.toArray());
        return comboBoxModel;
    }
    
    public DefaultComboBoxModel getDataComboBoxModel()
    {
        ArrayList<DataObject> data = new ArrayList();
        for( int i = 0; i < storage_.size() ; i++ )
        {
            if(storage_.get(i).getPosition() != 0)
            {
                data.add(storage_.get(i));
            }
        }
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(data.toArray());
        return comboBoxModel;
    }
    
    public DataObject getTimeDataForGivenChart(DataObject data)
    {
        String path = data.getFilePath();
        DataObject ret;
        for(int i = 0; i < storage_.size(); i++)
        {
            DataObject temp = storage_.get(i);
            if(temp.getFilePath() == path && temp.getPosition() == 0)
                return temp;
        }
        return storage_.get(0);
    }
}
