/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import CoreElements.Measurement;
import CoreElements.MeasurementReader;
import CoreElements.MeasurementTemplate;
import CoreElements.PropertiesReader;
import CoreElements.TemplateReader;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 *
 * @author norrec
 */
public class EntityController
{
    private static Vector<Measurement> measurements_;
    private static Vector<MeasurementTemplate> templates_;
    private static JPanel currentPanel_;
    private static MenuWindow mainPanel_;
    private static JPanel[] panels_;
    protected Hashtable dataBase = new Hashtable();
    final static protected JPanel contentPane = new JPanel();
    private static String appLocation_;
    
private static void initializeData()
{
    try
    {
    PropertiesReader pr_ = new PropertiesReader();
    appLocation_ = pr_.getDataLocation();
    checkIfDirectoriesExist();
    reloadData();
    }
    catch(Exception e)
    {
        
    }
}

private static void checkIfDirectoriesExist()
{
    File data = new File(appLocation_);
    File templates = new File(appLocation_ + "\\Measurements");
    File measurements = new File(appLocation_ + "\\Templates"); 
    if(!data.exists())
    {
        try
        {
            data.mkdir();
        } catch(SecurityException se){}   
    }
    if(!templates.exists())
    {
        try
        {
            templates.mkdir();
        } catch(SecurityException se){}
    }
    if(!measurements.exists())
    {
        try
        {
            measurements.mkdir();
        } catch(SecurityException se){}
    }          
}

private static void reloadData()
{
   MeasurementReader mr_ = new MeasurementReader(appLocation_);
   measurements_ = mr_.ReadMeasurementFiles();
   TemplateReader tr_ = new TemplateReader(appLocation_);
   templates_ = tr_.ReadTemplateFiles(); 
}

private static void createAndShowGUI()
    {
        JFrame frame = new JFrame("ChartViewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 

        contentPane.setLayout(new CardLayout());

        MenuWindow menuWindow = new MenuWindow(measurements_);
        menuWindow.setName("MainMenu");
        contentPane.add(menuWindow, "Main Menu");
        mainPanel_ = menuWindow;

        JPanel buttonPanel = new JPanel(); 
        
        final JButton AButton = new JButton("View");
        AButton.setForeground(Color.BLACK);
        
        final JButton CButton = new JButton("New");
        CButton.setForeground(Color.BLACK);
        
        final JButton DButton = new JButton("Main Menu");
        DButton.setForeground(Color.BLACK);
        
        final JButton EButton = new JButton("Options");
        EButton.setForeground(Color.BLACK);
        
        buttonPanel.add(AButton);
        buttonPanel.add(CButton);
        buttonPanel.add(DButton);
        buttonPanel.add(EButton);
        buttonPanel.setBackground(new Color(240, 240, 240));
        
        AButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                int selectedIndex = mainPanel_.getSelectedIndex();
                JPanel newPanel = new ViewPanel(measurements_.get(selectedIndex));
                newPanel.setName("a");
                contentPane.add(newPanel, "a");
                CardLayout cardLayout = (CardLayout) contentPane.getLayout();
                cardLayout.next(contentPane);
                currentPanel_ = newPanel;
            }
        });
        
        CButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                JPanel newPanel = new MeasurementEditor(templates_, appLocation_);
                contentPane.add(newPanel, "b");
                CardLayout cardLayout = (CardLayout) contentPane.getLayout();
                cardLayout.next(contentPane);
                currentPanel_ = newPanel;
            }
        });
        
        DButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                reloadData();
                CardLayout cardLayout = (CardLayout) contentPane.getLayout();
                Component[] components = contentPane.getComponents();
                for(int i = 1 ; i < components.length; i++)
                {
                    contentPane.remove(components[i]);
                    currentPanel_ = mainPanel_;
                }
                if(currentPanel_ != null)
                {
                    adjustButtons(currentPanel_.getName());
                    currentPanel_.revalidate();
                    currentPanel_.repaint();
                } 
            }
        });
        
        EButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                OptionsFrame of = new OptionsFrame();
                of.show();
            }
        });

        frame.add(contentPane, BorderLayout.CENTER);
        frame.add(buttonPanel, BorderLayout.PAGE_END);

        frame.pack();
        frame.setVisible(true);
    }

private static void adjustButtons(String currentPanel)
{
    return;
}

public static void main(String... args)
    {
        try {
        UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                initializeData();
                createAndShowGUI();
            }
        });
    }

}
