/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import CoreElements.Measurement;
import java.awt.Color;
import java.awt.Dimension;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;

/**
 *
 * @author norrec
 */
public class ViewPanel extends javax.swing.JPanel
{
    private Measurement measurement_;
    private int actualChartNumber_;
    /**
     * Creates new form ViewPanel
     */
    public ViewPanel(Measurement measurement)
    {
        actualChartNumber_ = 0;
        measurement_ = measurement;
        initComponents();
        //setPreferredSize(new Dimension(800, 600));
        createChartView();
        revalidateButtonStatus();
    }

    private void createChartView()
    {
        JFreeChart jfreechart = measurement_.getChartObject(actualChartNumber_).getJFreeChart();
        //XYPlot xyplot = (XYPlot) jfreechart.getPlot();
        //xyplot.getDomainAxis().setLowerMargin(0.0D);
        //xyplot.getDomainAxis().setUpperMargin(0.0D);
        //XYLineAndShapeRenderer xylineandshaperenderer = (XYLineAndShapeRenderer) xyplot.getRenderer();
        //xylineandshaperenderer.setLegendLine(new java.awt.geom.Rectangle2D.Double(-4D, -3D, 8D, 6D));       
        
        
        
        
    	//JFreeChart jfreechart = ChartFactory.createScatterPlot(null, null, null, measurement_.getChartObject(actualChartNumber_).getData(),getJFreeChart
        //    PlotOrientation.VERTICAL, false, true, false);
        XYPlot xyPlot = (XYPlot) jfreechart.getPlot();
        xyPlot.setDomainCrosshairVisible(true);
        xyPlot.setRangeCrosshairVisible(true);
        XYItemRenderer renderer = xyPlot.getRenderer();
        renderer.setSeriesPaint(0, Color.blue);
        NumberAxis domain = (NumberAxis) xyPlot.getDomainAxis();
        //domain.setRange(0.00, 100.00);
        domain.setAutoRange(true);
        //domain.setTickUnit(new NumberTickUnit(1));
        //domain.setVerticalTickLabels(true);
        NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
        //range.setRange(0.0, 100.0);
        range.setAutoRange(true);        
         	
    	ChartPanel chartPanel = new ChartPanel(jfreechart);
        chartPanel.setBackground(new Color(238, 238, 238));
        chartPanel.setForeground(Color.pink);
        chartPanel.setMaximumDrawHeight(1000);
        chartPanel.setMinimumDrawHeight(600);
        chartPanel.setMaximumDrawWidth(1000);
        chartPanel.setMinimumDrawWidth(600);
        chartPanel.setPopupMenu(null);
        chartPanel.setMouseWheelEnabled(true);
        chartPanel.setMouseZoomable(true);
        chartPanel.setDomainZoomable(true);
        chartPanel.setRangeZoomable(false);
        channelObjectPanel.removeAll();
        channelObjectPanel.add(chartPanel);
        channelObjectPanel.setVisible(true);
        channelObjectPanel.revalidate();
        channelObjectPanel.repaint();
        revalidate();
        repaint();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        nameLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        nextButton = new javax.swing.JButton();
        previousButton = new javax.swing.JButton();
        channelObjectPanel = new javax.swing.JPanel();
        chartDescription = new javax.swing.JLabel();

        setLayout(new java.awt.GridBagLayout());

        nameLabel.setText(measurement_.getName());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        add(nameLabel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 500;
        gridBagConstraints.ipady = 10;
        add(jSeparator1, gridBagConstraints);

        nextButton.setText(">");
        nextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(nextButton, gridBagConstraints);

        previousButton.setText("<");
        previousButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                previousButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(previousButton, gridBagConstraints);

        channelObjectPanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.gridheight = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(channelObjectPanel, gridBagConstraints);

        chartDescription.setText(measurement_.getChartObject(actualChartNumber_).getName());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.weightx = 1.0;
        add(chartDescription, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void previousButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_previousButtonActionPerformed
        actualChartNumber_--;
        createChartView();
        revalidateButtonStatus();
    }//GEN-LAST:event_previousButtonActionPerformed

    private void nextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextButtonActionPerformed
        actualChartNumber_++;
        createChartView();
        revalidateButtonStatus();
    }//GEN-LAST:event_nextButtonActionPerformed

    private void revalidateButtonStatus()
    {
        chartDescription.setText(measurement_.getChartObject(actualChartNumber_).getName());
        if(actualChartNumber_ == 0)
        {
            previousButton.setEnabled(false);
            nextButton.setEnabled(true);
        }
        else if(actualChartNumber_ == measurement_.getChartsSize() - 1 )
        {
            previousButton.setEnabled(true);
            nextButton.setEnabled(false);
        }
        else
        {
            previousButton.setEnabled(true);
            nextButton.setEnabled(true);
        }
        revalidate();
        repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel channelObjectPanel;
    private javax.swing.JLabel chartDescription;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JButton nextButton;
    private javax.swing.JButton previousButton;
    // End of variables declaration//GEN-END:variables
}
