/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

/**
 *
 * @author dmalek
 */
public class TreeNode
{
    public String fileName_;
    public String filePath_;
    
    public TreeNode(String fileName)
    {
        fileName_ = fileName;
        filePath_ = "";
    }
    public TreeNode(String fileName, String filePath)
    {
        fileName_ = fileName;
        filePath_ = filePath;
    }
    
    // HAVE TO IMPLEMENT TO STRING METHOD FOR DISPLAY PURPOSE
    public String toString()
    {
        return fileName_;
    }
}
