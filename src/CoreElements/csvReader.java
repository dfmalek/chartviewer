/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

/**
 *
 * @author norrec
 */

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

public class csvReader
{
  private static String fFilePath = null;
  private static ArrayList<List<Object>> mothership;
  private static int amountOfColumns;
  private static ArrayList<ArrayList<Double>> columns;

  public csvReader(String aFilePath)
  {
    fFilePath = aFilePath;
    mothership = new ArrayList();
    amountOfColumns = 0;
    columns = new ArrayList();
  }

  public static void readFile()
    throws Exception
  {
    ICsvListReader listReader = null;
    try {
      listReader = new CsvListReader(new FileReader(fFilePath), CsvPreference.EXCEL_PREFERENCE);

      listReader.getHeader(true);
      amountOfColumns = listReader.length();
      CellProcessor[] processor = new CellProcessor[amountOfColumns];
      List temp;
      while ((temp = listReader.read(processor)) != null)
      {
        mothership.add(temp);
      }
    }
    finally
    {
      if (listReader != null) {
        listReader.close();
        Transpond();
      }
    }
  }

  public ArrayList<ArrayList<Double>> GetData()
  {
    return columns;
  }

  public XYSeriesCollection getChart(int positionX, int positionY)
  {
    XYSeriesCollection dataset;
    XYSeries data = new XYSeries("wykres", false);
    for (int it = 0; it < ((ArrayList)columns.get(0)).size(); it++)
    {
      data.add(Double.parseDouble(((ArrayList)columns.get(positionX)).get(it).toString()), Double.parseDouble(((ArrayList)columns.get(positionY)).get(it).toString()));
    }
    XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
    xySeriesCollection.addSeries(data);
    dataset = xySeriesCollection;

    return dataset;
  }

  private static void Transpond()
  {
    for (int y = 0; y < amountOfColumns; y++)
    {
      ArrayList temp = new ArrayList();
      for (int i = 0; i < mothership.size(); i++)
      {
        temp.add(((List)mothership.get(i)).get(y));
      }
      columns.add(temp);
    }
  }
}
