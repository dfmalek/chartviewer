/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

import CoreElements.MathOperations.DFT;
import CoreElements.MathOperations.OperationType;
import CoreElements.MathOperations.SetOperation;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author dmalek
 */
public class TemplateReader
{
    private String templatesLocation_;
    
    public TemplateReader(String templatesLocation)
    {
        templatesLocation_ = templatesLocation + "\\Templates\\";
        System.out.println(templatesLocation_);
    }
    
    public Vector<MeasurementTemplate> ReadTemplateFiles()
    {
        Vector<MeasurementTemplate> measurements = new Vector<MeasurementTemplate>();
        File fileDirectory = new File(templatesLocation_);
        Vector<String> fileNames = getAllFileNames(fileDirectory);
        for(int i = 0; i < fileNames.size(); i++)
        {
            try
            {
                String temp = readFile(templatesLocation_ + fileNames.get(i), Charset.defaultCharset());
                measurements.add(parseFile(temp));
            }
            catch (IOException e)
            {
                System.err.println("Caught IOException: " + e.getMessage());
            }
            catch (Exception e)
            {
                System.err.println("File is corrupted! " + e.getMessage());
            }
        }
        return measurements;
    }
    
    private Vector<String> getAllFileNames(File folder)
    {
        Vector<String> fileNames = new Vector<String>();
        for (File fileEntry : folder.listFiles())
        {
            fileNames.add(fileEntry.getName());
        }
        return fileNames;
    }
    
    private String readFile(String path, Charset encoding)
        throws IOException 
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
    
    private MeasurementTemplate parseFile(String fileContent)
    {
        String[] lines = fileContent.split("\\r?\\n");      
        return new MeasurementTemplate(lines[1], parseChartTemplates(lines));
    }
    
    private Vector<ChartObjectTemplate> parseChartTemplates(String[] splittedFileContent)
    {
        Vector<ChartObjectTemplate> chartObjects = new Vector<ChartObjectTemplate>();
        boolean isInList = false;
        System.out.println(Arrays.toString(splittedFileContent));
        for(int i = 0; i < splittedFileContent.length; i++)
        {
            if(isInList == true)
            {
                if(splittedFileContent[i].contains("*"))
                {
                    System.out.println(chartObjects.size());
                    return chartObjects;
                }
                else if(splittedFileContent[i].contains("SetOperation"))
                {
                    String[] lines = splittedFileContent[i].split("\\|");
                    String[] linesA = lines[1].split(" ");
                    //ChartObject A = new ChartObject(linesA[0], Integer.parseInt(linesA[1]), Integer.parseInt(linesA[2])); TO DO
                    String[] linesB = lines[2].split(" ");
                    //ChartObject B = new ChartObject(linesB[0], Integer.parseInt(linesB[1]), Integer.parseInt(linesB[2]));
                    String[] linesSo = lines[3].split(" ");
                    IMathOperation setOperation = new SetOperation(
                            Double.parseDouble(linesSo[0]),
                            Double.parseDouble(linesSo[1]),
                            Double.parseDouble(linesSo[2]),
                            OperationType.getOperationType(Integer.parseInt(linesSo[3])));
                    ArrayList<XYSeriesCollection> dataAccumulation = new ArrayList();
                    //dataAccumulation.add(A.getData());
                    //dataAccumulation.add(B.getData());
                    Vector<IMathOperation> mathOperations = new Vector();
                    mathOperations.add(setOperation);
                    chartObjects.add(new ChartObjectTemplate(lines[0], Integer.parseInt(lines[1]), Integer.parseInt(lines[2]), mathOperations));
                }
                else
                {  
                    String[] temp = splittedFileContent[i].split("\\|");
                    String[] lines = temp[1].split(" ");
                    System.out.println(Arrays.toString(lines));
                    System.out.println(lines.length);
                    if(lines.length > 2)
                    {
                        chartObjects.add(new ChartObjectTemplate(temp[0], Integer.parseInt(lines[0]), Integer.parseInt(lines[1]), getMathOperationsFromString(lines[2])));
                    }
                    else
                    {
                        chartObjects.add(new ChartObjectTemplate(temp[0], Integer.parseInt(lines[0]), Integer.parseInt(lines[1]), getMathOperationsFromString("")));
                    }
                }
            }
            if(splittedFileContent[i].contains("*"))
            {
                isInList = true;
            }
        }
        System.out.println(chartObjects.size());
        return chartObjects;
    }
    
    private Vector<IMathOperation> getMathOperationsFromString(String operations)
    {
        Vector<IMathOperation> mathOperations = new Vector<IMathOperation>();
        operations = operations.replace("|", "");
        String[] tempOperations = operations.split(",");
        System.out.println(operations);
        for( int i = 0; i < tempOperations.length; i++)
        {
            if(tempOperations[i].equals("DFT"))
            {
                mathOperations.add(new DFT());
            }
            //if()
            // MORE OPERATIONS TO BE ADDED HERE
        }
        return mathOperations;
    }
}
