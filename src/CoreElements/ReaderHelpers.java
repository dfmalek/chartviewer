/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dmalek
 */
public class ReaderHelpers
{
    public static Vector<String> getDescriptionLine(String filePath)
    {
        String[] stringedDescriptionLine = null;
	BufferedReader bufferedReader = null;
	String descriptionLine = "";
	String cvsSplitBy = ",";
 
	try
        {
            bufferedReader = new BufferedReader(new FileReader(filePath));
            descriptionLine = bufferedReader.readLine();
            stringedDescriptionLine = descriptionLine.split(cvsSplitBy);
	}
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
	}
        catch (IOException e)
        {
            e.printStackTrace();
	}
        finally
        {
            if (bufferedReader != null)
            {
		try
                {
                    bufferedReader.close();
		}
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
	}
        
        return new Vector<String>(Arrays.asList(stringedDescriptionLine));
    }
    
    public static ArrayList<ArrayList<Double>> readFile(String filePath)
    {
        csvReader reader = new csvReader(filePath);
        try {
            reader.readFile();
        } catch (Exception ex) {
            Logger.getLogger(ReaderHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reader.GetData();
    }
  
    
}
