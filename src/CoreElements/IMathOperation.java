/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

import java.util.ArrayList;
import org.jfree.data.xy.XYSeriesCollection;

public abstract class IMathOperation
{
    public int coefficient_ = 0;

    abstract public XYSeriesCollection calculate(ArrayList<XYSeriesCollection> data);
    abstract public String toString();
}
