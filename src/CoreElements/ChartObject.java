/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

import Storage.DataObject;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import org.jfree.data.xy.*;
import java.util.Vector;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
/**
 *
 * @author norrec
 */
public class ChartObject
{  
    private String path_;
    private String name_;
    private int positionX_;
    private int positionY_;
    private Vector<IMathOperation> mathOperations_;
    private XYSeriesCollection data_;
    private ArrayList<XYSeriesCollection> clearData_;
    
    public ChartObject() 
    {
    }
    
    public ChartObject(String name, String path, int positionX, int positionY)
    {
        name_ = name;
        path_ = path;
        positionX_ = positionX;
        positionY_ = positionY;
        mathOperations_ = new Vector<IMathOperation>();
        getChartDataFromFile();
        prepareData();
    }
    
    public ChartObject(DataObject dataX, DataObject dataY)
    {
        path_ = dataX.getFilePath();
        mathOperations_ = new Vector<IMathOperation>();
        CreateChartDataBasedOnDataObjects(dataX.getData(), dataY.getData());
    }
    
    public ChartObject(String name, String path, int positionX, int positionY, Vector<IMathOperation> mathOperations)
    {
        name_ = name;
        path_ = path;
        positionX_ = positionX;
        positionY_ = positionY;
        mathOperations_ = new Vector<IMathOperation>(); //dafuq
        getChartDataFromFile();
        mathOperations_ = mathOperations;
        prepareData();
    }
    
    public ChartObject(String name, String path, int positionX, int positionY, XYSeriesCollection data, Vector<IMathOperation> mathOperations)
    {
        name_ = name;
        path_ = path;
        positionX_ = positionX;
        positionY_ = positionY;
        data_ = data;
        clearData_ = new ArrayList();
        clearData_.add(data);
        mathOperations_ = mathOperations;
        //prepareData();
    }
    
    public ChartObject(ArrayList<XYSeriesCollection> clearData, Vector<IMathOperation> mathOperations)
    {
        clearData_ = clearData;
        mathOperations_ = mathOperations;
        prepareData();
    }
    
    public String getName()
    {
        return name_;
    }
    
    public void setName(String name)
    {
        name_ = name;
    }
    
    public int getPositionX()
    {
        return positionX_;
    }
    
    public int getPositionY()
    {
        return positionY_;
    }
    
    public Vector<IMathOperation> getMathOperations()
    {
        return mathOperations_;
    }
    
    public JFreeChart getJFreeChart()
    {
        if(!isVectorAnalysis())
        {
            JFreeChart chart = ChartFactory.createXYLineChart(null, "X", "Y", data_, PlotOrientation.VERTICAL, true, true, false);
            return chart;
        }
        JFreeChart chart = ChartFactory.createScatterPlot(null, "X", "Y", data_, PlotOrientation.VERTICAL, true, true, false);
        Shape point = new Ellipse2D.Double(0,0,4,4);
        XYPlot xyplot = (XYPlot) chart.getXYPlot();
        XYItemRenderer renderer = xyplot.getRenderer();
        renderer.setShape(point);
        return chart;
    }
    
    public XYSeriesCollection getData()
    {
        return data_;
    }
    
    public ArrayList<XYSeriesCollection> getClearData()
    {
        return clearData_;
    }
    
    public String getPath()
    {
        return path_;
    }
    
    public String getFileString()
    {
        return new String(name_ + " |" + path_ + " " + positionX_ + " " + positionY_ + " |" + getMathOperationsString());
    }
    
    public String getTemplate()
    {
        return new String(name_ + " |" + positionX_ + " " + positionY_ + " |" + getMathOperationsString());
    }
    
    @Override
    public String toString()
    {
        return name_;
    }
    
    private void prepareData()
    {
        for(int i = 0; i < mathOperations_.size(); i++)
        {
            data_ = mathOperations_.elementAt(i).calculate(clearData_);
        }
    }
    
    private boolean isVectorAnalysis()
    {
        if(positionX_ == 0 || positionY_ == 0)
        {
            return false;
        }
        return true;
    }
    
    private String getMathOperationsString()
    {
        String mathOperationsString = new String();
        for( int i = 0; i < mathOperations_.size(); i++)
        {
            if(mathOperations_.get(i) != null)
            {
                mathOperationsString += (mathOperations_.get(i).toString() + " ");
            }
        }
        return mathOperationsString;
    }
    
    private void getChartDataFromFile()
    {
        try
        {
            csvReader parser = new csvReader(path_);
            parser.readFile();
            data_ = parser.getChart(positionX_, positionY_);
            clearData_ = new ArrayList();
            clearData_.add(data_);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void CreateChartDataBasedOnDataObjects(ArrayList<Double> dataX, ArrayList<Double> dataY)
    {
        XYSeriesCollection dataset;
        XYSeries data = new XYSeries("wykres", false);
        for (int it = 0; it < dataX.size(); it++)
        {
          data.add(Double.parseDouble(dataX.get(it).toString()), Double.parseDouble(dataY.get(it).toString()));
        }
        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
        xySeriesCollection.addSeries(data);
        data_ = xySeriesCollection;

        clearData_ = new ArrayList();
        clearData_.add(data_);
    }
    
}
