/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

import java.util.ArrayList;
import java.util.Vector;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author dmalek
 */
public class ChartObjectTemplate
{
    private String name_;
    private int positionX_;
    private int positionY_;
    private Vector<IMathOperation> mathOperations_;
    
    ChartObjectTemplate(ChartObject chartObject)
    {
        name_ = chartObject.getName();
        positionX_ = chartObject.getPositionX();
        positionY_ = chartObject.getPositionY();
        mathOperations_ = chartObject.getMathOperations();
    }
    
    public ChartObjectTemplate(String name, int positionX, int positionY, Vector<IMathOperation> mathOperations)
    {
        name_ = name;
        positionX_ = positionX;
        positionY_ = positionY;
        mathOperations_ = new Vector<IMathOperation>(); //dafuq
        mathOperations_ = mathOperations;
    }
    
    private String getMathOperationsString()
    {
        String mathOperationsString = new String();
        for( int i = 0; i < mathOperations_.size(); i++)
        {
            if(mathOperations_.get(i) != null)
            {
                mathOperationsString += (mathOperations_.get(i).toString() + " ");
            }
        }
        return mathOperationsString;
    }
    
    public String getName()
    {
        return name_;
    }

    public String getFileString(String path)
    {
        return new String(name_ + " |" + path + " " + positionX_ + " " + positionY_ + " |" + getMathOperationsString());
    }
    
}
