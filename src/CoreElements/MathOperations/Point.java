/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements.MathOperations;

/**
 *
 * @author dmalek
 */
public class Point
{
    public double x;
    public double y;
    
    public Point(double X, double Y)
    {
        x = X;
        y = Y;
    }
}
