/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements.MathOperations;

import CoreElements.IMathOperation;
import java.util.ArrayList;
import java.util.Iterator;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author dmalek
 */
public class SetOperation extends IMathOperation
{
    private double squareSizeX_;
    private double squareSizeY_;
    private double densityLimit_;
    private OperationType operationType_;
    private ArrayList<ChartSquare> AChart_;
    private ArrayList<ChartSquare> BChart_;
    private String saveString_;
    
    public SetOperation(double squareSizeX, double squareSizeY, double densityLimit, OperationType operationType)
    {
        squareSizeX_ = squareSizeX;
        squareSizeY_ = squareSizeY;
        densityLimit_ = densityLimit;
        operationType_ = operationType;
    }
    
    public XYSeriesCollection calculate(ArrayList<XYSeriesCollection> data)
    {
        AChart_ = prepareData(data.get(0));
        BChart_ = prepareData(data.get(1));
        switch (operationType_)
        {
            case Union:
                return createSeries(AChart_, BChart_);
        }
        executeOperation();
        return createSeries(AChart_);
        //XYSeries xy = new XYSeries("ziemniak");
        //return new XYSeriesCollection(xy);
    }
    
    public void setOperationType(OperationType operationType)
    {
        operationType_ = operationType;
    }
    
    public OperationType getOperationType()
    {
        return operationType_;
    }
    
    public void setSquareSizeX(int squareSize)
    {
        squareSizeX_ = squareSize;
    }
    
    public double getSquareSizeX()
    {
        return squareSizeX_;
    }
    
    public void setSquareSizeY(int squareSize)
    {
        squareSizeY_ = squareSize;
    }
    
    public double getSquareSizeY()
    {
        return squareSizeY_;
    }
    
    public void setDensityLimit(double densityLimit)
    {
        densityLimit_ = densityLimit;
    }
    
    public double getDensityLimit()
    {
        return densityLimit_;
    }
    
    public void setSaveString(String saveString)
    {
        saveString_ = saveString;
    }
    
    public String toString()
    {   
        return new String(saveString_ + " |" + squareSizeX_ + " " +
                squareSizeY_ + " " + densityLimit_ + " " + operationType_.returnInt() +
                " SetOperation");
    }
    
    private void executeOperation()
    {
        switch (operationType_)
        {
            case Intersection:
                AChart_.retainAll(BChart_);
                break;
            case Subtraction:
                AChart_.removeAll(BChart_);
                break;
            case Union:
                AChart_.addAll(BChart_);
                break;
        }
                
        
    }
    
    private XYSeriesCollection createSeries(ArrayList<ChartSquare> squareChart)
    {
        XYSeries xySeries = new XYSeries("setOperation");
        //XYSeriesCollection ret = new XYSeriesCollection();
        for(ChartSquare cs : squareChart)
        {
            for(Point point : cs.points)
            {
                xySeries.add(point.x, point.y);
            }
        }
        
        XYSeriesCollection ret = new XYSeriesCollection(xySeries);
        return ret;
    }
    
    private XYSeriesCollection createSeries(ArrayList<ChartSquare> squareChartOne,
            ArrayList<ChartSquare> squareChartTwo)
    {
        XYSeries xySeries = new XYSeries("setOperation");
      
        for(ChartSquare cs : squareChartOne)
        {
            for(Point point : cs.points)
            {
                xySeries.add(point.x, point.y);
            }
        }
        
        XYSeriesCollection ret = new XYSeriesCollection(xySeries);
        
        XYSeries secondSet = new XYSeries("secondSet");
      
        for(ChartSquare cs : squareChartTwo)
        {
            for(Point point : cs.points)
            {
                secondSet.add(point.x, point.y);
            }
        }
        
        ret.addSeries(secondSet);
        return ret;
    }
    
    private ArrayList<ChartSquare> prepareData(XYSeriesCollection data)
    {
        XYSeries seriesData = data.getSeries(0);
        double minX = seriesData.getMinX();
        double minY = seriesData.getMinY();
        double maxX = seriesData.getMaxX();
        double maxY = seriesData.getMaxY();
        int rows = (int)Math.ceil((maxY - minY)/squareSizeY_);
        int cols = (int)Math.ceil((maxX - minX)/squareSizeX_);
        
        ArrayList<ChartSquare> preparedData = new ArrayList<ChartSquare>();
                
        for(int i = 0; i < rows ; i++)
        {
            for(int j = 0; j < cols ; j++)
            {
                preparedData.add(new ChartSquare(j, i));
            }
        }
        
        for(int i = 0; i < seriesData.getItemCount(); i++)
        {
            double x = seriesData.getX(i).doubleValue();
            double y = seriesData.getY(i).doubleValue();
            int posX = (int)(x/squareSizeX_);
            int posY = (int)(y/squareSizeY_);
            int correctionX = (int)(-minX/squareSizeX_);
            int correctionY = (int)(-minY/squareSizeY_);
            int resultPosition = (posX + correctionX) + (posY + correctionY) * cols;
            if(resultPosition < preparedData.size() && resultPosition >= 0)
                preparedData.get(resultPosition).points.add(new Point(x, y));
            else
                System.out.println("O KURCZE");
        }
        /* algorithm debug logs
        for (ChartSquare result1 : preparedData)
        {
            System.out.println(result1.points.size() + " " + result1.positionX + " " + result1.positionY);
        }*/
        int maxSize = 0;
        for ( ChartSquare chartSquare : preparedData)
        {
            if(chartSquare.points.size() > maxSize)
            {
                maxSize = chartSquare.points.size();
            }
        }
        for (ChartSquare resultElement : preparedData)
        {
            if(!resultElement.points.isEmpty() &&
                resultElement.points.size() < (0.01 * densityLimit_ * maxSize))
            {
                /*debug log*/
                System.out.println("Element at position : (" +
                    resultElement.positionX + ", " + resultElement.positionY +
                    ") did not reach densityLimit!" );
                /**/
                resultElement.points.clear();
            }
        }
        System.out.println("Data prepared!");
        return removeEmptyElements(preparedData);
    }
    
    private ArrayList<ChartSquare> removeEmptyElements(ArrayList<ChartSquare> toBeCleared)
    {
        for (Iterator<ChartSquare> iterator = toBeCleared.iterator(); iterator.hasNext();)
        {
            ChartSquare cs = iterator.next();
            if (cs.points.isEmpty())
            {
                iterator.remove();
            }
        }
        return toBeCleared;
    }
    
}
