/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements.MathOperations;

import CoreElements.IMathOperation;
import static java.lang.Math.abs;
import java.util.ArrayList;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jtransforms.fft.DoubleFFT_1D;


/**
 *
 * @author dmalek
 */
public class DFT extends IMathOperation
{

    public XYSeriesCollection calculate(ArrayList<XYSeriesCollection> data)
    {
        XYSeriesCollection dataFFT = data.get(0);
        int n = dataFFT.getItemCount(0);
        double dt = dataFFT.getXValue(0,1) - dataFFT.getXValue(0, 0);
        double fp = 1/dt;
        double[] inreal = new double[n];
        for(int i = 0; i < n; i++)
        {
            inreal[i] = dataFFT.getYValue(0, i);
        }
        DoubleFFT_1D fftJob = new DoubleFFT_1D(n);
        double[] fft = new double[n*2];
        System.arraycopy(inreal, 0, fft, 0, n);
        fftJob.realForward(fft);
        XYSeriesCollection dataset;
        XYSeries newData = new XYSeries("wykres");
        for (int it = 0; it < n; it++)
        {
            newData.add(it*fp/(n) ,abs(fft[it]));
        }
        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
        xySeriesCollection.addSeries(newData);
        dataset = xySeriesCollection;

        return dataset;
    }
    
    public String toString()
    {   
        return new String("DFT|");
    }
}
