/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements.MathOperations;

/**
 *
 * @author dmalek
 */
public enum OperationType
{
    Intersection(1), Subtraction(2), Union(3);
        private int value;
        
        public static OperationType getOperationType(int x) {
        switch(x) {
        case 1:
            return Intersection;
        case 2:
            return Subtraction;
        case 3:
            return Union;
        }
        return null;
    }
        public int returnInt()
        {
            return value;
        }

        private OperationType(int value) {
                this.value = value;
        }

}
