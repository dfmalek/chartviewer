/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements.MathOperations;

import java.util.ArrayList;

/**
 *
 * @author dmalek
 */
public class ChartSquare
{
    public int positionX;
    public int positionY;
    public ArrayList<Point> points;
    
    ChartSquare(int posX, int posY)
    {
        positionX = posX;
        positionY = posY;
        points = new ArrayList<Point>();
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass()) return false;
        ChartSquare other = (ChartSquare) obj;
        if(positionX == other.positionX && positionY == other.positionY)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
