/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

import Storage.DataStorage;
import java.util.ArrayList;

/**
 *
 * @author norrec
 */
public class ChartManager
{
    private DataStorage dataStorage_;
    private Measurement measurement_;
    private ArrayList<ChartObject> chartObjects_;
    

    public ChartManager(Measurement measurement, ArrayList<String> files)
    {
        measurement_ = measurement;
        dataStorage_ = new DataStorage(files);
        chartObjects_ = new ArrayList();
        //ChartObject cO = new ChartObject(dataStorage_.getDataObject(1), dataStorage_.getDataObject(2));
        //addChart(cO);
    }
    
    public ArrayList<ChartObject> getAllCharts()
    {
        return chartObjects_;
    }
    
    public int getSize()
    {
        return chartObjects_.size();
    }
    
    public void removeChart(int position)
    {
        chartObjects_.remove(position);
    }
    
    public void addChart(ChartObject chartToBeAdded)
    {
        chartObjects_.add(chartToBeAdded);
    }
    
    public DataStorage getDataStorage()
    {
        return dataStorage_;
    }
    
    public Measurement getMeasurement()
    {
        return measurement_;
    }
    
}
