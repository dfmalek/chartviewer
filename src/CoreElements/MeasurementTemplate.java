/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

import java.util.Vector;

/**
 *
 * @author dmalek
 */
public class MeasurementTemplate
{
    private Vector<ChartObjectTemplate> chartObjectTemplates_;
    private String name_;
    
    MeasurementTemplate(String name, Vector<ChartObjectTemplate> chartObjectTemplates)
    {
        name_ = name;
        chartObjectTemplates_ = chartObjectTemplates;
    }
    
    public Vector<ChartObjectTemplate> getChartObjectsTemplates()
    {
        return chartObjectTemplates_;
    }
    
    public String toString()
    {
        return name_;
    }
    
    public int getSize()
    {
        return chartObjectTemplates_.size();
    }
    
    
    
}