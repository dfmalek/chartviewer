/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;


import java.util.*;
import javax.swing.JTree;
/**
 *
 * @author norrec
 */
public class Measurement
{
    private String name_;
    private String description_;
    private Vector<ChartObject> charts_;
    
    public Measurement()
    {
        name_ = "";
        description_ = "";
        charts_ = new Vector();
    }
    
    public Measurement(String name, String description,
            Vector<ChartObject> charts)
    {
        name_ = name;
        description_ = description;
        charts_ = charts;
    }
    
    public Vector<String> getMeasurementFiles()
    {
        Set<String> files = new HashSet<String>();
        for(int i = 0; i < charts_.size(); i++)
        {
            System.out.println(charts_.get(i).getPath());
            files.add(charts_.get(i).getPath());
        }
        return new Vector<String>(files);
    }
    
    public Vector<String> getDescriptionLine()
    {
        Vector<String> descriptionLine = new Vector<String>();
        for( int i = 0; i < 5; i++)
        {
            descriptionLine.add("element" + i);
        }
        return descriptionLine;
    }
    
    public String getName()
    {
        return name_;
    }
    
    public void setName(String name)
    {
        name_ = name;
    }
    
    public String getDescription()
    {
        return description_;
    }
    
    public void setDescription(String description)
    {
        description_ = description;
    }
    
    public ChartObject getChartObject(int position)
    {
        System.out.println(position);
        return charts_.get(position);
    }
    
    public void addChartObject(ChartObject chartObject)
    {
        charts_.add(chartObject);
    }
    
    public void setChartObject(ChartObject chartObject, int position)
    {
        charts_.set(position, chartObject);
    }
    
    public int getChartsSize()
    {
        return charts_.size();
    }
    
    public boolean save()
    {
        // How to save single measurement to filed
        //x name
        //x Description
        //x *(star - starts files) chartObject. getFileString() for each! 
        //x /Data/klaczka_01_klus01.p0.csv 1 |
        //x /Data/klaczka_01_klus01.p1.csv 1 |
        //x *(star - ends chart files and whole file)
        
        return true;
    }
    
}
