/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreElements;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author dmalek
 */
public class PropertiesReader
{
    public String getDataLocation() throws IOException // can be expanded to handle much more properties. Now it is hardcoded :(
    {
        String configLocation = System.getProperty("user.dir") + "/config.ini";
        File f = new File(configLocation);
        if(!f.exists())
        {
            saveDefaultProperties();
        }
        Properties properties = new Properties();
        FileInputStream inputStream = new FileInputStream(configLocation);//"D:\\userdata\\dmalek\\Downloads\\INZ\\dfmalek-engineerproject-1a52a551f294\\dfmalek-engineerproject-1a52a551f294\\config.ini");
        if (inputStream != null)
        {
            properties.load(inputStream);
        }
        else
        {
            return System.getProperty("user.dir") + "/Data/";
        }
        
        String dataLocation = properties.getProperty("dataLocation");
        return dataLocation;
    }
    
    public void saveProperties(String dataLocation)  // for now there is no need for structs or sth like that!
    {
        Properties properties = new Properties();
	OutputStream output = null;
 
	try {
 
		output = new FileOutputStream("config.ini");
 
		properties.setProperty("dataLocation", dataLocation);
 
		properties.store(output, null);
 
	} catch (IOException io) {
		io.printStackTrace();
	} finally {
		if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
 
	}
    }
    
    
    private void saveDefaultProperties()
    {
        Properties properties = new Properties();
	OutputStream output = null;
 
	try {
 
		output = new FileOutputStream("config.ini");
 
		properties.setProperty("dataLocation", System.getProperty("user.dir")+"\\Data");
 
		properties.store(output, null);
 
	} catch (IOException io) {
		io.printStackTrace();
	} finally {
		if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
 
	}
    }
    
}
